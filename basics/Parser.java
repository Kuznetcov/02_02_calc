package basics;

import java.util.HashMap;

public class Parser {

    static HashMap<String, Integer> dump = new HashMap<String, Integer>();

    public static String parse(String exp) {
        //System.out.print(exp);
        int ind = 0;
        int ind2 = 0;
        int indInt = 0;
        int ind2Int = 0;
        int count = 0;
        String subExp = "";

        StringBuffer rezult = new StringBuffer(exp);
        String subExpInt = rezult.toString();

        for (int i = 0; i < exp.length(); i++) {
            char c = exp.charAt(i);

            if (count != 0) {
                subExp = subExp + c;
            }
            if (c == '(' && count == 0) {
                ind = i;
                count++;
                subExp = "" + c;
            } else if (c == '(' && count == 1) {
                count++;
                indInt = i;
            } else if (c == '(' && count > 1) {
                count++;
            } else if (c == ')' && count > 1) {
                count--;
                ind2Int = i;

                subExpInt = rezult.toString();
                String s = subExpInt.substring(subExpInt.substring(0, i).lastIndexOf("("), i + 1); // получаем вложенную вложенную скобку.
                
                System.out.println("Наша основная создавая скобка " + subExp);
                System.out.println("Обрабатываемая скобка " + s);
                while (rezult.indexOf(s) != -1) {
                    rezult = rezult.replace(rezult.indexOf(s), (rezult.indexOf(s) + s.length()), Parser.parse(s));
                }
                System.out.println("REZULT " + rezult);
                System.out.println("SUBEXPINT " + subExpInt);

            } else if (c == ')' && count == 1) {
                count--;
                ind2 = i;
                if (indInt != 0 && ind2Int != 0) { //Если найдена последняя скобка, а внутри был вложенный блок скобок, то этот блок заменяется на его значение
                    subExpInt = subExp.replace(exp.substring(indInt, ind2Int + 1), Parser.parse(exp.substring(indInt, ind2Int + 1)));
                }

                //найденную скобку заносим в коллекцию,
                if ("".equals(subExpInt)) {
                    subExpInt = subExp;
                }
                Integer evalRezult;
                if (dump.get(subExpInt) != null) {
                    evalRezult = dump.get(subExpInt);
                } else {
                    evalRezult = Polsk.eval(subExpInt);
                }
                dump.put(subExp, evalRezult);
                while (rezult.indexOf(subExp) != -1) {
                    rezult = rezult.replace(rezult.indexOf(subExp), (rezult.indexOf(subExp) + subExp.length()), Integer.toString(evalRezult));
                }
                ind = 0;
                ind2 = 0;
                subExpInt = "";

            }

        }

        if (subExp.equals("")) {
            subExp = exp;
        }

        dump.put(exp, Polsk.eval(rezult.toString()));
        //System.out.println (" вернули: "+Integer.toString(Polsk.eval(subExp)));
        return (Integer.toString(Polsk.eval(subExp)));
    }
}
