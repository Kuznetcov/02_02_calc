package basics;

import java.util.Arrays;
import java.util.HashSet;

public class Validate {

  String exp;
  String info;
  boolean correct = true;
  static HashSet<Character> chars = new HashSet<Character>(Arrays.asList('1', '2', '3', '4', '5',
      '6', '7', '8', '9', '0', '+', '-', '/', '*', '(', ')', ' '));

  public Validate(String exp) {
    this.exp = exp;
    this.info = "";
    check(exp);
  }

  private void check(String exp) {

    StringBuilder s = new StringBuilder(exp);

    for (int i = 0; i < exp.length(); i++) {

      if (exp.charAt(i) == '.') {
        this.info = "������� ����� ���� �� �������������� \n";
      }

      if (!chars.contains(exp.charAt(i))) {
        s.setCharAt(i, '^');
        correct = false;
      }

    }
    if (correct == false) {
      this.info += "Error in exp:\n" + exp + "\n" + s + "\n";
    }
  }
}
