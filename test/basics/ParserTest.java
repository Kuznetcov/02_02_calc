package basics;


import static org.junit.Assert.*;

import org.junit.Test;

public class ParserTest {

  @Test
  public void parseWithoutBrackets() {
    @SuppressWarnings("unused")
    Parser2 parser = new Parser2();
    String result = Parser2.parse("5 + 5 * -5 / 5 - 10");
    assertEquals("-10", result);
  }
  
  @Test
  public void parseWithBrackets() {
    @SuppressWarnings("unused")
    Parser2 parser = new Parser2();
    String result = Parser2.parse("(((10-5)) * ((1-6)*(-5+-5)))");
    assertEquals("250", result);
  }

}
