package basics;

import org.junit.Test;

public class CalculatorTest {

  @Test
  public void calculateTrueEval(){
    @SuppressWarnings("unused")
    Calculator cal = new Calculator();
    String source = "5+5";
    Calculator.calculate(source);
  }
  
  @Test
  public void calculateDivisionZero(){
    
    String source = "5/0";
    Calculator.calculate(source);
  }
  
  @Test
  public void calculateWrongEval(){
    
    String source = "5+2.5";
    Calculator.calculate(source);
  }
  
  @Test
  public void calculateChars(){
    
    String source = "A + B";
    Calculator.calculate(source);
  }
  
  @Test
  public void calculateDoubleOp(){
    
    String source = "5 **5";
    Calculator.calculate(source);
  }
  
}
